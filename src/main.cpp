
#include <iostream>
#include <f/f.h>
#include <g/g.h>

using namespace std;

int main(int argc, char*argv[]) {
  f::doThingF();
  g::doThingG();
}
